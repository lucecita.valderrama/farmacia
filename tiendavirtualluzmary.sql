CREATE DATABASE  IF NOT EXISTS `tiendavirtual4` /*!40100 DEFAULT CHARACTER SET swe7 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `tiendavirtual4`;
-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: tiendavirtual4
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `compras`
--

DROP TABLE IF EXISTS `compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `compras` (
  `id_producto` int NOT NULL AUTO_INCREMENT,
  `nombre_producto` varchar(45) DEFAULT NULL,
  `cantidad_compra` int DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `total_compra` double DEFAULT NULL,
  `fecha_compra` varchar(45) DEFAULT NULL,
  `tipos_usuario_id_tipo_usuario` int NOT NULL,
  PRIMARY KEY (`id_producto`,`tipos_usuario_id_tipo_usuario`),
  KEY `fk_compras_tipos_usuario1_idx` (`tipos_usuario_id_tipo_usuario`),
  CONSTRAINT `fk_compras_tipos_usuario1` FOREIGN KEY (`tipos_usuario_id_tipo_usuario`) REFERENCES `tipos_usuario` (`id_tipo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=swe7;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compras`
--

LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventario`
--

DROP TABLE IF EXISTS `inventario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `inventario` (
  `id_producto` int NOT NULL AUTO_INCREMENT,
  `nombre_producto` varchar(45) DEFAULT NULL,
  `cantidad_final` int DEFAULT NULL,
  PRIMARY KEY (`id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=swe7;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventario`
--

LOCK TABLES `inventario` WRITE;
/*!40000 ALTER TABLE `inventario` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto_bd`
--

DROP TABLE IF EXISTS `producto_bd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `producto_bd` (
  `id_producto` int NOT NULL AUTO_INCREMENT,
  `nombre_producto` varchar(45) DEFAULT NULL,
  `precio_compra` double DEFAULT NULL,
  `categoria` varchar(45) DEFAULT NULL,
  `tipos_usuario_id_tipo_usuario` int NOT NULL,
  PRIMARY KEY (`id_producto`,`tipos_usuario_id_tipo_usuario`),
  KEY `fk_producto_bd_tipos_usuario1_idx` (`tipos_usuario_id_tipo_usuario`),
  CONSTRAINT `fk_producto_bd_tipos_usuario1` FOREIGN KEY (`tipos_usuario_id_tipo_usuario`) REFERENCES `tipos_usuario` (`id_tipo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=swe7;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto_bd`
--

LOCK TABLES `producto_bd` WRITE;
/*!40000 ALTER TABLE `producto_bd` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto_bd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiendas`
--

DROP TABLE IF EXISTS `tiendas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tiendas` (
  `id_tiendas` int NOT NULL AUTO_INCREMENT,
  `cc_propietario` int NOT NULL,
  `nombre_tienda` varchar(45) DEFAULT NULL,
  `nombre_propietario` varchar(45) DEFAULT NULL,
  `apellido_propietario` varchar(45) DEFAULT NULL,
  `usuario_id_usuario` int NOT NULL,
  `tipos_usuario_id_tipo_usuario` int NOT NULL,
  PRIMARY KEY (`id_tiendas`,`cc_propietario`,`usuario_id_usuario`,`tipos_usuario_id_tipo_usuario`),
  KEY `fk_tiendas_usuario_idx` (`usuario_id_usuario`),
  KEY `fk_tiendas_tipos_usuario1_idx` (`tipos_usuario_id_tipo_usuario`),
  CONSTRAINT `fk_tiendas_tipos_usuario1` FOREIGN KEY (`tipos_usuario_id_tipo_usuario`) REFERENCES `tipos_usuario` (`id_tipo_usuario`),
  CONSTRAINT `fk_tiendas_usuario` FOREIGN KEY (`usuario_id_usuario`) REFERENCES `usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=swe7;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiendas`
--

LOCK TABLES `tiendas` WRITE;
/*!40000 ALTER TABLE `tiendas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiendas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipos_usuario`
--

DROP TABLE IF EXISTS `tipos_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipos_usuario` (
  `id_tipo_usuario` int NOT NULL AUTO_INCREMENT,
  `administrador` varchar(45) DEFAULT NULL,
  `propietario` varchar(45) DEFAULT NULL,
  `vendedor` varchar(45) DEFAULT NULL,
  `comprador` varchar(45) DEFAULT NULL,
  `inventario_id_producto` int NOT NULL,
  PRIMARY KEY (`id_tipo_usuario`,`inventario_id_producto`),
  KEY `fk_tipos_usuario_inventario1_idx` (`inventario_id_producto`),
  CONSTRAINT `fk_tipos_usuario_inventario1` FOREIGN KEY (`inventario_id_producto`) REFERENCES `inventario` (`id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=swe7;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipos_usuario`
--

LOCK TABLES `tipos_usuario` WRITE;
/*!40000 ALTER TABLE `tipos_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipos_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `id_usuario` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `contrasena` varchar(45) DEFAULT NULL,
  `fecha_creacion` int DEFAULT NULL,
  `tipos_usuario_id_tipo_usuario` int NOT NULL,
  `tipos_usuario_inventario_id_producto` int NOT NULL,
  PRIMARY KEY (`id_usuario`,`tipos_usuario_id_tipo_usuario`,`tipos_usuario_inventario_id_producto`),
  KEY `fk_usuario_tipos_usuario1_idx` (`tipos_usuario_id_tipo_usuario`,`tipos_usuario_inventario_id_producto`),
  CONSTRAINT `fk_usuario_tipos_usuario1` FOREIGN KEY (`tipos_usuario_id_tipo_usuario`, `tipos_usuario_inventario_id_producto`) REFERENCES `tipos_usuario` (`id_tipo_usuario`, `inventario_id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=swe7;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventas`
--

DROP TABLE IF EXISTS `ventas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ventas` (
  `id_producto` int NOT NULL AUTO_INCREMENT,
  `nombre_producto` varchar(45) DEFAULT NULL,
  `fecha_venta` varchar(45) DEFAULT NULL,
  `cantidad_vendida` double DEFAULT NULL,
  `precio_venta` double DEFAULT NULL,
  `total_venta` double DEFAULT NULL,
  `tipos_usuario_id_tipo_usuario` int NOT NULL,
  PRIMARY KEY (`id_producto`,`tipos_usuario_id_tipo_usuario`),
  KEY `fk_ventas_tipos_usuario1_idx` (`tipos_usuario_id_tipo_usuario`),
  CONSTRAINT `fk_ventas_tipos_usuario1` FOREIGN KEY (`tipos_usuario_id_tipo_usuario`) REFERENCES `tipos_usuario` (`id_tipo_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=swe7;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventas`
--

LOCK TABLES `ventas` WRITE;
/*!40000 ALTER TABLE `ventas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ventas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tiendavirtual4'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-02 18:51:23
